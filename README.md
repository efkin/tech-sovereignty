README
======

What is this all about?
-----------------------
This is an introduction to Technological Sovereignty that will be presented at Jardin Entropique.


How is organized the repo?
--------------------------
### src/ ###
it contains the material on which the presentation is based

### notes.md ###
it contains some notes about the readings

### structure.md ###
it contains the structure of the presentation

Can I Contribute ?
------------------
Even if still the repo is a big mess, contributions are welcome.


