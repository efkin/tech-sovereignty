Structure
=========

Introduction
------------
* What is this conference about?

    Basically it is a comparison between two historical experiences of TS.

1. PHRACK: when the TS was still undifferentiated expierience, a descriptive problem, a discovery journey.
2. TS Dossier: when TS is a *positive* experience, that has conscious of it self, that writes its own definition, an argumentative problem, a politic journey.

* How this conference was prepared?

    http://gitlab.com/efkin/tech-sovereignty


Motivations
-----------
* Why PHRACK? 
    * (little contextualization)
    * why the firsts 8 issues?
    * Illegalization process of bbs's
* Why TSD? 
    * (little contextualization)
    * we wanted to write our own pubblications, and we just started doing it.


2 Manifestos, 1 Lemma
---------------------
* Techno-Revolution by Dr.Crash
* Index of TSD and spideralex's TS definition.
* Summary and comparison

In-depth comparison
-------------------
* Primitivity of PHRACK
* Positivity of TSD

Conclusion and Open questions
------------------------------
