Notes regarding Phrack
======================

### 7 ###
Issue #6 - phile 12 of 13
Federal laws for bbs
1. It forbids unauthorized access to a computer and drops a requirement that
   the government prove information in the computer was used or altered.

2. It outlaws "pirate bulletin boards" used by hackers to trade secret computer
   codes and passwords.

3. It makes it a felony punishable by up to five years in prison and a $250,000
   fine to maliciously cause damage in excess of $1,000 to a computer program
   or data base.

### 6 ###
Issue #6 - phile 3 of 13
Techno-revolution by Dr.Crash - june 10th 1986
* candidate to contrast spideralex manifesto about TS.
* The computer system has been solely in the hands of big businesses and the government.  
* The wonderful device meant to enrich life has become a weapon which dehumanizes people.  
* To the government and large businesses, **people are no more than disk space**.
* the government doesn't use computers to arrange aid for the poor, but to control
nuclear death weapons.
* The average American can only have access to a small microcomputer which is worth only a fraction of what they pay for it. 
* It is because of this state of affairs that hacking was born.
* The phone company is another example of
technology abused and kept from people with high prices.
* Carding is a way of obtaining the necessary goods without paying for
them.
* Anarchy as the process of physically destroying buildings and governmental establishments.
* Whether you know it or not, if you are a
hacker, you are a revolutionary. 


### 5 ###
Issue #6 - phile 2 of 13
Groups pro-phile
* possibly comparable with maxigas article -section hacklab.


### 4 ###
Issue #5 - phile 12 of 12
Interview
"It says that the sysops "organized, financed, directed, and oversaw the
    illicit posting and trading of Teltec codes"  "They failed to delete the
    messages containing illegal information."  
"You see so the sysops are guilty
    cause they didn't delete the messages."

### 3 ###
Issue #4 - phile 6 of 11 by The Mentor
"Occasionally there will be a time when destruction is necessary.
Whether it is revenge against a tyrannical system operator or against
a particular company, sometimes it is desirable to strike at the heart of a
company...their computer."


### 2 ###
Issue #3 - phile 9 of 10
Introduction to PBXs
"This is a very brief description of how to use and what to expect on a PBX."
* Possibly related to note #1


### 1 ###
Issue #2 - phile 2 of 9
* "Don't Shit in the woods I LIVE HERE!" - 
* "So if someone tries to add something onto your line you will be notified before hand. This has two advantages, one you will prevent any occurences on your line, two you will know that someone is attempting to mess around with your phone line.""
* Can this be primitive technological sovereignty?
* also related issue #5 - phile 7 of 12 - describes arpanet.


### 0 ###
* Why Phrack?
    I tried to find in the public domain publications that zero degree in the history of technological sovereignty, which is undifferentiated experience.
* This is not in any case a history of TS, maybe just an analysis of the rudimentary movements of an experience.
* As by notes #1 and #2, we can read how the relation with sorrounding technology (PBXs, ARPANET) is a discovery process, a descriptive problem. "let's find out how that works". in TS, instead, we found a politic process, an argumentative problem. "we know how that works, let's find TS"
* As by the second quote of note #1, we found a maybe primitive idea fo TS. probably opposable to definition of TS.
* Referring to note #3 we found  "as little as the fear that you feel, as big as the enemy you choose". They had an enemy, they had no fear. we could argue that this primal experience has a big connotation of destruction with an end in itself. This can be opposable to TS, that has a big connotation of construction of alternatives.
* 